<div id="top"></div>


<h3 align="center">Nuvalence Rectangle Take Home Project</h3>

<!-- TABLE OF CONTENTS -->

  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#getting-started">Getting Started</a>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>




<!-- ABOUT THE PROJECT -->
<div id="about-the-project"></div>

## About The Project

[Project Outline][project_outline]

The Nuvalence Rectangle Take Home Project is a coding challenge that looks to implement certain algorithms that
analyze rectangles and features that exist among rectangles. The 3 given algorithms that were required were **Intersection**, **Containment**, and **Adjacency**. However, I also implemented a **equality** algorithm as well.

Note: In the project I refer to Containment as Encapsulation. 

<p align="right">(<a href="#top">back to top</a>)</p>


<div id="built-with"></div>

### Built With

* [Java 17](https://www.oracle.com/java/technologies/downloads/)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/)


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
<div id="getting-started"></div>

## Getting Started

### Prerequisites

Download the project, normally this would be hosted on gitlab however since this is a company's interviewing question  it will not be posted to a publicly available gitlab repo

### Installation

1. after navigating to project folder then clean install 
   ```sh
   mvn clean install
   ```
2. mvn verify (optional)
   ```sh
   mvn verify
   ```
3. run project
   ```sh
   mvn spring-boot:run 
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
<div id="usage"></div>

## Usage

Once you have the project running, via mvn spring-boot:run, you will be presented with a menu 

```
Menu:
1 - Use default rectangle values 
2 - Input values
3 - Show Menu
4 - quit

```

### Option 1: Use default rectangle values

Choosing this will run through a series of different Rectangle combinations. 

Ten in total to include two error input examples.

#### First Rectangle Pair 
![Rectangle Pair][default1]

Results:
- are they the same?: false
- is there intersection?: false
- is there encapsulation?: false
- is there adjacency?: true

Why no encapsulation? Given that in the [Project Outline][project_outline] it did not specifically show this situation as containment I considered it an edge case. With this being the case I needed to decided if "Containment" meant that all points for blue rectangle need to be inside the red rectangle. There is definitely an argument to be made that blue points on a red line would still invoke containment since those points are not technically outside the red rectangle. However, I when considering a user I think they would consider containment to mean all blue points must be entirely inside the red rectangle without touching. 

#### Second Rectangle Pair
![Rectangle Pair][default2]

Results:
- are they the same?: false
- is there intersection?: true
- is there encapsulation?: false
- is there adjacency?: false


#### Third Rectangle Pair
![Rectangle Pair][default3]

Results:
- are they the same?: false
- is there intersection?: true
- is there encapsulation?: false
- is there adjacency?: false

#### Forth Rectangle Pair
![Rectangle Pair][default4]

Results:
- are they the same?: false
- is there intersection?: false
- is there encapsulation?: true
- is there adjacency?: false

#### Fifth Rectangle Pair
![Rectangle Pair][default5]

Results:
- are they the same?: false
- is there intersection?: false
- is there encapsulation?: false
- is there adjacency?: true

#### Sixth Rectangle Pair
![Rectangle Pair][default6]

Results:
- are they the same?: false
- is there intersection?: false
- is there encapsulation?: false
- is there adjacency?: true

#### Seventh Rectangle Pair
![Rectangle Pair][default7]

Results:
- are they the same?: false
- is there intersection?: false
- is there encapsulation?: false
- is there adjacency?: true

#### Eighth Rectangle Pair
![Rectangle Pair][default8]

Results:
- are they the same?: false
- is there intersection?: false
- is there encapsulation?: false
- is there adjacency?: false

#### Ninth Rectangle Pair: Error Case 1
![Rectangle Pair][default9]

Results:

Wrong number of cords passed

#### Tenth Rectangle Pair: Error Case 2
![Rectangle Pair][default10]

Results:

Bad Cords passed


### Option 2: Input values

Choosing this option will allow you to input your own values for Rectangle one and/or Rectangle Two

The Base Rectangle is the Red Rectangle that is used in option 1. With cords 
1. (0,0)
2. (10,0)
3. (10,20)
4. (0,20)

When entering Cords the expected input is either: x,y or (x,y) However, I implemented a regex to find the cords in the input string that is passed. So, entering something like "Dev9,0Bot" is still valid and will be parsed as 9,0

After entering your chosen cords you will have the option to preform each test on your Rectangle pairs.


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
<div id="roadmap"></div>

## Roadmap for v2

- [] Push to private Gitlab repo
- [] Create API
- [] Create React Front End
- [] Host on AWS
    - [] Add subpage to personal site (Front End)
    - [] Host engine on EC2 Server (Back End)


<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTACT -->
<div id="contact"></div>

## Contact

Devin Wallace

Email: DevinWallace@protonmail.com

Phone: (309)205-1983

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
[project_outline]: ./public/pdf/Rectangles%20Programming%20Sample.pdf
[default1]: ./public/img/default1.png
[default2]: ./public/img/default2.png
[default3]: ./public/img/default3.png
[default4]: ./public/img/default4.png
[default5]: ./public/img/default5.png
[default6]: ./public/img/default6.png
[default7]: ./public/img/default7.png
[default8]: ./public/img/default8.png
[default9]: ./public/img/default9ec1.png
[default10]: ./public/img/default10ec2.png