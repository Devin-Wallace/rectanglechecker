package com.rectangle.Utility;

import com.rectangle.Classes.Rectangle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/*
    tests for the Rectangle Utility Class
 */
class RectangleCheckerTest {

    /*
        Test to validate that you can pull point1 from the defined rectangle
        Test:
            if actual.getP1() == {0, 0}
                test passed
     */
    @Test
    void encapsulationCheck() {
        int[][] baseCords = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        int[][] testCords = {{1, 2}, {9, 2}, {9, 19}, {1, 2}};
        Rectangle base = new Rectangle(baseCords);
        Rectangle tester = new Rectangle(testCords);
        Assertions.assertTrue(RectangleChecker.encapsulationCheck(base, tester));

        tester.setCords(new int[][] {{12, 1}, {17, 1}, {17, 22}, {12, 22}});
        Assertions.assertFalse(RectangleChecker.encapsulationCheck(base, tester));
    }

    /*
        Test to validate that you can pull point1 from the defined rectangle
        Test:
            if actual.getP1() == {0, 0}
                test passed
     */
    @Test
    void intersectionCheck() {
        int[][] baseCords = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        int[][] testCords = {{5, 2}, {12, 2}, {12, 19}, {5, 19}};
        Rectangle base = new Rectangle(baseCords);
        Rectangle tester = new Rectangle(testCords);
        Assertions.assertTrue(RectangleChecker.intersectionCheck(base, tester));

        tester.setCords(new int[][] {{12, 1}, {17, 1}, {17, 22}, {12, 22}});
        Assertions.assertFalse(RectangleChecker.intersectionCheck(base, tester));
    }

    /*
        Test to validate that you can pull point1 from the defined rectangle
        Test:
            if actual.getP1() == {0, 0}
                test passed
     */
    @Test
    void equalityCheck() {
        int[][] baseCords = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        int[][] testCords = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        Rectangle base = new Rectangle(baseCords);
        Rectangle tester = new Rectangle(testCords);
        Assertions.assertTrue(RectangleChecker.equalityCheck(base, tester));

        tester.setCords(new int[][] {{12, 1}, {17, 1}, {17, 22}, {12, 22}});
        Assertions.assertFalse(RectangleChecker.equalityCheck(base, tester));
    }

    /*
        Test to validate that you can pull point1 from the defined rectangle
        Test:
            if actual.getP1() == {0, 0}
                test passed
     */
    @Test
    void adjacencyCheck() {
        int[][] baseCords = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        int[][] testCords = {{10, 1}, {15, 1}, {15, 22}, {10, 22}};
        Rectangle base = new Rectangle(baseCords);
        Rectangle tester = new Rectangle(testCords);
        Assertions.assertTrue(RectangleChecker.adjacencyCheck(base, tester));

        tester.setCords(new int[][] {{10, 2}, {15, 2}, {15, 19}, {10, 19}});
        Assertions.assertTrue(RectangleChecker.adjacencyCheck(base, tester));

        tester.setCords(new int[][] {{10, 0}, {15, 0}, {15, 20}, {10, 20}});
        Assertions.assertTrue(RectangleChecker.adjacencyCheck(base, tester));

        tester.setCords(new int[][] {{12, 1}, {17, 1}, {17, 22}, {12, 22}});
        Assertions.assertFalse(RectangleChecker.adjacencyCheck(base, tester));
    }

    /*
        Test to validate that you can pull point1 from the defined rectangle
        Test:
            if actual.getP1() == {0, 0}
                test passed
     */
    @Test
    void getResults() {
        int[][] baseCords = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        int[][] testCords = {{3, 15}, {10, 15}, {10, 20}, {3, 20}};
        Rectangle base = new Rectangle(baseCords);
        Rectangle tester = new Rectangle(testCords);
        String expected = "are they the same?: false\n" +
                "is there intersection?: false\n" +
                "is there encapsulation?: false\n" +
                "is there adjacency?: true\n";
        ByteArrayOutputStream os = new ByteArrayOutputStream(112);
        PrintStream capture = new PrintStream(os);
        System.setOut(capture);
        RectangleChecker.getResults(base, tester);
        capture.flush();
        String result = os.toString();
        System.setOut(System.out);
        Assertions.assertEquals(expected, result, "getResults Test");
    }
}