package com.rectangle.Classes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
/*
    tests for the Rectangle Class
 */
public class RectangleTest {

    /*
        Test to validate that you can pull point1 from the defined rectangle
        Test:
            if actual.getP1() == {0, 0}
                test passed
     */
    @Test
    public void testGetP1() {
        Rectangle actual = new Rectangle(new int[][]{{0, 0}, {10, 0}, {10, 20}, {0, 20}});
        int[][] expected = new int[][] {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        Assertions.assertArrayEquals(expected[0], actual.getP1());
    }

    /*
        Test to validate that you can pull point2 from the defined rectangle
        Test:
            if actual.getP2() == {10, 0}
                test passed
     */
    @Test
    public void testGetP2() {
        Rectangle actual = new Rectangle(new int[][]{{0, 0}, {10, 0}, {10, 20}, {0, 20}});
        int[][] expected = new int[][] {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        Assertions.assertArrayEquals(expected[1], actual.getP2());
    }

    /*
        Test to validate that you can pull point3 from the defined rectangle
        Test:
            if actual.getP3() == {10, 20}
                test passed
     */
    @Test
    public void testGetP3() {
        Rectangle actual = new Rectangle(new int[][]{{0, 0}, {10, 0}, {10, 20}, {0, 20}});
        int[][] expected = new int[][] {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        Assertions.assertArrayEquals(expected[2], actual.getP3());
    }

    /*
        Test to validate that you can pull point4 from the defined rectangle
        Test:
            if actual.getP4() == {0, 20}
                test passed
     */
    @Test
    public void testGetP4() {
        Rectangle actual = new Rectangle(new int[][]{{0, 0}, {10, 0}, {10, 20}, {0, 20}});
        int[][] expected = new int[][] {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        Assertions.assertArrayEquals(expected[3], actual.getP4());
    }

    /*
        Test to validate that you can get all coordinate points from the defined rectangle
        Test:
            if actual.getCords() == {{0, 0}, {10, 0}, {10, 20}, {0, 20}}
                test passed
     */
    @Test
    public void testGetCords() {
        Rectangle actual = new Rectangle(new int[][]{{0, 0}, {10, 0}, {10, 20}, {0, 20}});
        int[][] expected = new int[][] {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        Assertions.assertArrayEquals(expected, actual.getCords());
    }

    /*
        Test to validate that you can set a new set of cords for a defined rectangle
        Test:
            define actual rectangle with oldCords
            if actual.getCords() == oldCords
                test passed
            call actual.setCords(newCords)
            if actual.getCords() == newCords
                test passed
            if actual.getCords() != oldCords
     */
    @Test
    public void testSetCords() {
        int[][] oldCords = new int[][] {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
        Rectangle actual = new Rectangle(oldCords);
        Assertions.assertArrayEquals(oldCords, actual.getCords());
        int[][] newCords = new int[][] {{3, 15}, {10, 15}, {10, 20}, {3, 20}};
        actual.setCords(newCords);
        Assertions.assertArrayEquals(newCords, actual.getCords());
        Assertions.assertNotSame(newCords, actual.getCords());
    }

    /*
        Test to validate that inputted cords are valid
        Test:
            when a new Rectangle is defined, in the constructor validateCords(); is called which then sets
            private boolean validCords to either True or False, isValidCords() retrieves validCords
            if inputted cords is True
                test passed
            if inputted cords are False
                test passed
     */
    @Test
    public void testIsValidCords() {
        Rectangle actualTrue = new Rectangle(new int[][]{{0, 0}, {10, 0}, {10, 20}, {0, 20}});
        Assertions.assertTrue(actualTrue.isValidCords());
        Rectangle actualFalse = new Rectangle(new int[][]{});
        Assertions.assertFalse(actualFalse.isValidCords());
    }

    /*
        When a user inputs a new set of cords it comes in as a String int pair ie: "42,42"
        this test that those String int pairs can be parsed into actual int pairs
        Test:
            given "0,0" as actual
            if Rectangle.parsePoints(actual)
            is equal to {0,0}
            then test passed
     */
    @Test
    public void testParsePoints() {
        String actual = "0,0";
        int[] expected = {0,0};
        Assertions.assertArrayEquals(expected, Rectangle.parsePoints(actual));
    }

    /*
        Test to validate that you can print points in the correct format
        Test:
            given "P1: 0,0\nP2: 10,0\nP3: 10,20\nP4: 0,20" as expected
            (
            P1: 0,0
            P2: 10,0
            P3: 10,20
            P4: 0,20
            )

            if actual.printPoints == expected
                test passed
     */
    @Test
    public void testPrintPoints() {
        Rectangle actual = new Rectangle(new int[][]{{0, 0}, {10, 0}, {10, 20}, {0, 20}});
        String expected = "P1: 0,0\nP2: 10,0\nP3: 10,20\nP4: 0,20";
        ByteArrayOutputStream os = new ByteArrayOutputStream(35);
        PrintStream capture = new PrintStream(os);
        System.setOut(capture);
        actual.printPoints();
        capture.flush();
        String result = os.toString();
        System.setOut(System.out);
        Assertions.assertEquals(expected, result, "test");

    }
}