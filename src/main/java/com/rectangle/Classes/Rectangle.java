package com.rectangle.Classes;

import java.util.regex.*;

public class Rectangle {
    private int[][] cords = new int[4][2];
    private int[] p1 = new int[2];
    private int[] p2 = new int[2];
    private int[] p3 = new int[2];
    private int[] p4 = new int[2];
    private boolean validCords = false;

    public Rectangle(int[][] inputCords){
        try{
        // point 1 [0][0] == x cord. [0][1] == y cord
        cords[0][0] = inputCords[0][0];
        cords[0][1] = inputCords[0][1];
        p1[0] = cords[0][0];
        p1[1] = cords[0][1];

        // point 2 [1][0] == x cord. [1][1] == y cord
        cords[1][0] = inputCords[1][0];
        cords[1][1] = inputCords[1][1];
        p2[0] = cords[1][0];
        p2[1] = cords[1][1];

        // point 3 [2][0] == x cord. [2][1] == y cord
        cords[2][0] = inputCords[2][0];
        cords[2][1] = inputCords[2][1];
        p3[0] = cords[2][0];
        p3[1] = cords[2][1];

        // point 4 [3][0] == x cord. [3][1] == y cord
        cords[3][0] = inputCords[3][0];
        cords[3][1] = inputCords[3][1];
        p4[0] = cords[3][0];
        p4[1] = cords[3][1];

        validateCords();
        }catch(IllegalArgumentException e){
            System.out.println("Bad cords passed");
        }catch(ArrayIndexOutOfBoundsException ignored){
            System.out.println("Wrong number of cords passed");
        }

    }

    /*
        Checks to make sure provided cords are valid by working clockwise from p1 to p4
        Verifies no cords are equal to each other
        Verifies the cords share either a x-cord or a y-cord respectively
     */
    private boolean validateCords() {
        //if no cords are equal to each other
        if( p1 != p2 && p1 != p3 && p1 != p4 && p2 != p3 && p2 != p4 && p3 != p4){

            //if cords share either a x-cord or a y-cord respectively
            if(p1[1] == p2[1] && p2[0] == p3[0] && p3[1] == p4[1]) {
                validCords = true;
                return true;
            }

        }

        validCords = false;
        return false;
    }

    public int[] getP1() {
        return p1;
    }

    public int[] getP2() {
        return p2;
    }

    public int[] getP3() {
        return p3;
    }

    public int[] getP4() {
        return p4;
    }

    public int[][] getCords() {
        return cords;
    }

    public void setCords(int[][] inputCords) throws IllegalArgumentException{
        try{
        // point 1 [0][0] == x cord. [0][1] == y cord
        cords[0][0] = inputCords[0][0];
        cords[0][1] = inputCords[0][1];
        p1[0] = cords[0][0];
        p1[1] = cords[0][1];

        // point 2 [1][0] == x cord. [1][1] == y cord
        cords[1][0] = inputCords[1][0];
        cords[1][1] = inputCords[1][1];
        p2[0] = cords[1][0];
        p2[1] = cords[1][1];

        // point 3 [2][0] == x cord. [2][1] == y cord
        cords[2][0] = inputCords[2][0];
        cords[2][1] = inputCords[2][1];
        p3[0] = cords[2][0];
        p3[1] = cords[2][1];

        // point 4 [3][0] == x cord. [3][1] == y cord
        cords[3][0] = inputCords[3][0];
        cords[3][1] = inputCords[3][1];
        p4[0] = cords[3][0];
        p4[1] = cords[3][1];


        validateCords();
        }catch(IllegalArgumentException e){
            System.out.println("Bad cords passed");
        }catch(ArrayIndexOutOfBoundsException ignored){
            System.out.println("Wrong number of cords passed");
        }


    }

    public boolean isValidCords() {
        return validCords;
    }

    public static int[] parsePoints(String pointString) throws IllegalArgumentException{
        Pattern pointPattern = Pattern.compile("(\\d{1,},\\d{1,})");
        Matcher pointMatcher = pointPattern.matcher(pointString);
        if(pointMatcher.matches()){
            String[] outputStrings = pointString.split(",");
            return new int[]{Integer.parseInt(outputStrings[0]),Integer.parseInt(outputStrings[1])};
        }else{
            throw new IllegalArgumentException("IllegalArgumentException: Supplied user String is not in correct format");
        }
    }

    public void printPoints(){
        System.out.print(
                "P1: " + p1[0] + "," + p1[1] + "\n" +
                "P2: " + p2[0] + "," + p2[1] + "\n" +
                "P3: " + p3[0] + "," + p3[1] + "\n" +
                "P4: " + p4[0] + "," + p4[1]);
    }


}
