package com.rectangle;

import com.rectangle.Utility.RectangleChecker;
import com.rectangle.Classes.Rectangle;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        int userInput = 0;
        String menu = """
                        Menu:
                        1 - Use default rectangle values
                        2 - Input values
                        3 - Show Menu
                        4 - quit
                        """;
        String menu2 = """
                        Menu:
                        1 - print points
                        2 - test intersection
                        3 - test encapsulation
                        4 - test adjacency
                        5 - test equality
                        6 - quit
                        """;

        while(userInput != 4){
            
            if(userInput != 3){
                System.out.print(menu);
            }
            userInput = inputScanner.nextInt();
            switch (userInput) {
                case (1) -> {
                    System.out.println("default values one:");
                    int[][] cords1 = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};
                    int[][] cords2 = {{3, 15}, {10, 15}, {10, 20}, {3, 20}};
                    Rectangle r1 = new Rectangle(cords1);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println("");
                    Rectangle r2 = new Rectangle(cords2);
                    System.out.println("Rectangle2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.print("Press ENTER to continue...\n");
                    inputScanner.nextLine();
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values two:");
                    int[][] newCords = {{5, 2}, {12, 2}, {12, 19}, {5, 19}};
                    r2.setCords(newCords);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println();
                    System.out.println("r2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values three:");
                    newCords = new int[][]{{-5, 1}, {-5, 22}, {15, 22}, {15, 1}};
                    r2.setCords(newCords);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println();
                    System.out.println("r2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values four:");
                    newCords = new int[][]{{1, 2}, {9, 2}, {9, 19}, {1, 2}};
                    r2.setCords(newCords);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println();
                    System.out.println("r2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values five:");
                    newCords = new int[][]{{10, 2}, {15, 2}, {15, 19}, {10, 19}};
                    r2.setCords(newCords);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println();
                    System.out.println("r2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values six:");
                    newCords = new int[][]{{10, 0}, {15, 0}, {15, 20}, {10, 20}};
                    r2.setCords(newCords);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println();
                    System.out.println("r2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values seven:");
                    newCords = new int[][]{{10, 1}, {15, 1}, {15, 22}, {10, 22}};
                    r2.setCords(newCords);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println();
                    System.out.println("r2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values eight:");
                    newCords = new int[][]{{12, 1}, {17, 1}, {17, 22}, {12, 22}};
                    r2.setCords(newCords);
                    System.out.println("r1 cords");
                    r1.printPoints();
                    System.out.println();
                    System.out.println("r2 cords");
                    r2.printPoints();
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values nine error case 1:");
                    newCords = new int[][]{{5, 2}, {12, 2}, {12, 19}};
                    r2.setCords(newCords);
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
//----------------------------------------------------------------------------------------------------------------------
                    System.out.println("default values ten error case 2:");
                    newCords = new int[][]{{5, 2}, {12, 2}, {12, 19}};
                    r2.setCords(newCords);
                    System.out.println();
                    RectangleChecker.getResults(r1, r2);
                    System.out.println("Press ENTER to continue...");
                    inputScanner.nextLine();
                    System.out.println();
                }
//----------------------------------------------------------------------------------------------------------------------
                case (2) -> {
                    System.out.println("Would you like to use the base Rectangle? (Y/N)");
                    inputScanner.nextLine();
                    String input = inputScanner.nextLine();
                    Rectangle rectangle1;
                    if (input.equalsIgnoreCase("Y")) {
                        int[][] baseCords = {{0, 0}, {10, 0}, {10, 20}, {0, 20}};

                        rectangle1 = new Rectangle(baseCords);
                        System.out.println("Using base Rectangle...");
                    }else{
                        System.out.println("Enter points for Rectangle one using x,y format...");
                        System.out.println("EX: p1: 3,4");
                        System.out.println("p1: ");
                        String p1 = inputScanner.nextLine();
                        System.out.println("p2: ");
                        String p2 = inputScanner.nextLine();
                        System.out.println("p3: ");
                        String p3 = inputScanner.nextLine();
                        System.out.println("p4: ");
                        String p4 = inputScanner.nextLine();

                        rectangle1 = new Rectangle(new int[][]{Rectangle.parsePoints(p1), Rectangle.parsePoints(p2), Rectangle.parsePoints(p3), Rectangle.parsePoints(p4)});
                    }
                    System.out.println("Enter points for Rectangle two using x,y format...");
                    System.out.println("EX: p1: 3,4");
                    System.out.println("p1: ");
                    String p1 = inputScanner.nextLine();
                    System.out.println("p2: ");
                    String p2 = inputScanner.nextLine();
                    System.out.println("p3: ");
                    String p3 = inputScanner.nextLine();
                    System.out.println("p4: ");
                    String p4 = inputScanner.nextLine();

                    Rectangle rectangle2 = new Rectangle(new int[][]{Rectangle.parsePoints(p1), Rectangle.parsePoints(p2), Rectangle.parsePoints(p3), Rectangle.parsePoints(p4)});
                    System.out.println("Rectangle2 set...");
                    while(userInput < 6){
                        System.out.print(menu2);
                        userInput = inputScanner.nextInt();
                        switch (userInput){
                            case (1) -> {
                                System.out.println("rectangle1 cords");
                                rectangle1.printPoints();
                                System.out.println();
                                System.out.println("rectangle2 cords");
                                rectangle2.printPoints();
                                System.out.println();
                                System.out.println("Press ENTER to continue...");
                                inputScanner.nextLine();
                                inputScanner.nextLine();
                                System.out.println();
                            }
                            case (2) -> {
                                System.out.println("is there intersection?: " + RectangleChecker.intersectionCheck(rectangle1, rectangle2));
                                System.out.println("Press ENTER to continue...");
                                inputScanner.nextLine();
                                inputScanner.nextLine();
                                System.out.println();
                            }
                            case (3) -> {
                                System.out.println("is there encapsulation?: " + RectangleChecker.encapsulationCheck(rectangle1, rectangle2));
                                System.out.println("Press ENTER to continue...");
                                inputScanner.nextLine();
                                inputScanner.nextLine();
                                System.out.println();
                            }
                            case (4) -> {
                                System.out.println("is there adjacency?: " + RectangleChecker.adjacencyCheck(rectangle1, rectangle2));
                                System.out.println("Press ENTER to continue...");
                                inputScanner.nextLine();
                                inputScanner.nextLine();
                                System.out.println();
                            }
                            case (5) -> {
                                System.out.println("are the rectangles equal?: " + RectangleChecker.equalityCheck(rectangle1, rectangle2));
                                System.out.println("Press ENTER to continue...");
                                inputScanner.nextLine();
                                inputScanner.nextLine();
                                System.out.println();
                            }
                        }
                    }
                }
                case (3) -> System.out.print(menu);
                case (4) -> {
                    inputScanner.close();
                    System.exit(0);
                }
            }
        }
    }
}