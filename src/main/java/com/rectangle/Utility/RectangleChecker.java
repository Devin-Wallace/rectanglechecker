package com.rectangle.Utility;

import com.rectangle.Classes.Rectangle;
import java.util.Arrays;

public final class RectangleChecker {

//----------------------------------------------------------------------------------------------------------------------
    private static int[][] getMinMax(Rectangle rectangle){
        int[] XMinMax = {Math.min(rectangle.getP1()[0], rectangle.getP2()[0]), Math.max(rectangle.getP1()[0], rectangle.getP2()[0])};
        int[] YMinMax = {Math.min(rectangle.getP1()[1], rectangle.getP4()[1]), Math.max(rectangle.getP1()[1], rectangle.getP4()[1])};

        return new int[][]{XMinMax, YMinMax};
    }
//----------------------------------------------------------------------------------------------------------------------
    public static boolean encapsulationCheck(Rectangle r1, Rectangle r2){
        int[] r1XMinMax = getMinMax(r1)[0];
        int[] r1YMinMax = getMinMax(r1)[1];
        int[][] cords = r2.getCords();

        if(areAllPointInsideBoardersHelper(cords, r1XMinMax, r1YMinMax)) return true;

        int[] r2XMinMax = getMinMax(r2)[0];
        int[] r2YMinMax = getMinMax(r2)[1];
        cords = r1.getCords();

        return(areAllPointInsideBoardersHelper(cords, r2XMinMax, r2YMinMax));

    }
//----------------------------------------------------------------------------------------------------------------------
    public static boolean intersectionCheck(Rectangle r1, Rectangle r2){
        if(encapsulationCheck(r1,r2)) return false;

        int[] r1XMinMax = getMinMax(r1)[0];
        int[] r1YMinMax = getMinMax(r1)[1];
        int[][] cords = r2.getCords();


        if (isPointInsideBoardersHelper(cords, r1XMinMax, r1YMinMax) && isPointOutsideBoardersHelper(cords, r1XMinMax, r1YMinMax)) return true;

        int[] r2XMinMax = getMinMax(r2)[0];
        int[] r2YMinMax = getMinMax(r2)[1];
        cords = r1.getCords();

        //checking to see if there are two corresponding r2 points above and below OR left and right of r1,
        //if true then there is intersection
        return isPointInsideBoardersHelper(cords, r2XMinMax, r2YMinMax);
    }

    private static boolean isPointInsideBoardersHelper(int[][] cords, int[] XMinMax, int[] YMinMax) {
        for(int i = 0; i < 4; i++){
            // if a Rectangle point is between both the min and max X && Y values of another Rectangle then we know there must be intersection
            if((cords[i][0] > XMinMax[0] && cords[i][0] < XMinMax[1]) && (cords[i][1] > YMinMax[0] && cords[i][1] < YMinMax[1])){
                return true;
            }
        }
        return false;
    }

    private static boolean isPointOutsideBoardersHelper(int[][] cords, int[] XMinMax, int[] YMinMax) {
        for(int i = 0; i < 4; i++){
            // if a Rectangle point is between both the min and max X && Y values of another Rectangle then we know there must be intersection
            if((cords[i][0] < XMinMax[0] || cords[i][0] > XMinMax[1]) || (cords[i][1] < YMinMax[0] || cords[i][1] > YMinMax[1])){
                return true;
            }
        }
        return false;
    }

    private static boolean areAllPointInsideBoardersHelper(int[][] cords, int[] XMinMax, int[] YMinMax) {
        for(int i = 0; i < 4; i++){
            // if a Rectangle point is between both the min and max X && Y values of another Rectangle then we know there must be intersection
            if(!((cords[i][0] > XMinMax[0] && cords[i][0] < XMinMax[1]) && (cords[i][1] > YMinMax[0] && cords[i][1] < YMinMax[1]))){
                return false;
            }
        }
        return true;
    }
//----------------------------------------------------------------------------------------------------------------------
    public static boolean equalityCheck(Rectangle r1, Rectangle r2) {
        int[][] r1Cords = r1.getCords();
        int[][] r2Cords = r2.getCords();
        return Arrays.deepEquals(r1Cords, r2Cords);
    }
//----------------------------------------------------------------------------------------------------------------------
    public static boolean adjacencyCheck(Rectangle r1, Rectangle r2){
        int[] r1XMinMax = getMinMax(r1)[0];
        int[] r1YMinMax = getMinMax(r1)[1];
        int[] r2XMinMax = getMinMax(r2)[0];
        int[] r2YMinMax = getMinMax(r2)[1];

        int[][] r1Cords = r1.getCords();
        int[][] r2Cords = r2.getCords();


        if(adjacencyProper(r2Cords,r1XMinMax,r1YMinMax)) return true;
        if(adjacencyProper(r1Cords,r2XMinMax,r2YMinMax)) return true;

        if(adjacencySubLine(r2Cords,r1XMinMax,r1YMinMax)) return true;
        if(adjacencySubLine(r1Cords,r2XMinMax,r2YMinMax)) return true;

        if(adjacencyPartial(r2Cords,r1XMinMax,r1YMinMax)) return true;
        if(adjacencyPartial(r1Cords,r2XMinMax,r2YMinMax)) return true;

        return false;
    }

    private static boolean adjacencyProper(int[][] cords,int[] XMinMax,int[] YMinMax){
        for(int i = 0; i < 4; i++){
            if((cords[i][0] == XMinMax[0] && cords[i][1] == YMinMax[0]) || (cords[i][0] == XMinMax[0] && cords[i][1] == YMinMax[1]) ){
                for(int j = 0; j < 4; j++){
                    if((cords[j][0] == XMinMax[1] && cords[j][0] == YMinMax[0]) || (cords[j][0] == XMinMax[1] && cords[j][0] == YMinMax[1])){
                        if(cords[j] != cords[i]){
                            return true;
                        }
                    }
                }
            }
        }
        for(int i = 0; i < 4; i++){
            if((cords[i][1] == YMinMax[0] && cords[i][0] == XMinMax[0]) || (cords[i][1] == YMinMax[1] && cords[i][0] == XMinMax[0])){
                for(int j = 0; j < 4; j++){
                    if((cords[j][1] == YMinMax[1] && cords[i][0] == XMinMax[0]) || (cords[j][1] == YMinMax[1] && cords[i][0] == XMinMax[1])){
                        if(cords[j] != cords[i]){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private static boolean adjacencyPartial(int[][] cords,int[] XMinMax,int[] YMinMax){
        for(int i = 0; i < 4; i++){
            if((cords[i][1] == YMinMax[1] || cords[i][1] == YMinMax[0]) && (cords[i][0] > XMinMax[0] && cords[i][0] < XMinMax[1])){
                return true;
            }
        }
        for(int i = 0; i < 4; i++){
            if((cords[i][0] == XMinMax[1] || cords[i][0] == XMinMax[0]) && (cords[i][0] > YMinMax[0] && cords[i][0] < YMinMax[1])){
                return true;
            }
        }
        return false;

    }

    private static boolean adjacencySubLine(int[][] cords,int[] XMinMax,int[] YMinMax){
        for(int i = 0; i < 4; i++){
            if((cords[i][1] == YMinMax[1] || cords[i][1] == YMinMax[0]) && (cords[i][0] > XMinMax[0] && cords[i][0] < XMinMax[1])){
                for(int j = 0; j < 4; j++){
                    if(cords[j][1] == YMinMax[1] && (cords[j][0] > XMinMax[0] && cords[j][0] < XMinMax[1])){
                        if(cords[j] != cords[i]){
                            return true;
                        }
                    }
                }
            }
        }
        for(int i = 0; i < 4; i++){
            if((cords[i][0] == XMinMax[1] || cords[i][0] == XMinMax[0]) && (cords[i][0] > YMinMax[0] && cords[i][0] < YMinMax[1])){
                for(int j = 0; j < 4; j++){
                    if((cords[i][0] == XMinMax[1] || cords[i][0] == XMinMax[0]) && (cords[j][0] > YMinMax[0] && cords[j][0] < YMinMax[1])){
                        if(cords[j] != cords[i]){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
//----------------------------------------------------------------------------------------------------------------------

    public static void getResults(Rectangle r1, Rectangle r2) {
        System.out.print("are they the same?: " + RectangleChecker.equalityCheck(r1, r2) + "\n");
        System.out.print("is there intersection?: " + RectangleChecker.intersectionCheck(r1, r2) + "\n");
        System.out.print("is there encapsulation?: " + RectangleChecker.encapsulationCheck(r1, r2) + "\n");
        System.out.print("is there adjacency?: " + RectangleChecker.adjacencyCheck(r1, r2) + "\n");
    }
}
//----------------------------------------------------------------------------------------------------------------------
